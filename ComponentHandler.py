#execution testing module
import circuit
import component
import numpy as np

cir = circuit.cir("name", np.zeros((1,1)), [component.supply(0, v=1.5)])
cir.addComponent(component.resistor(1, r=1), 0, 0)
cir.printConMatrix()
cir.solveMissingInfo()