import ohm

s = 0
r = 1
d = 2

class comp:
    v = -1.0   #keeping as volts
    i = -1.0   #keeping as current A
    r = -1.0   #keeping as resistance Ohms
    name = "component"
    id=-1
    type = -1
    def __init__(self, name, id, type):
        self.name = name
        self.id = id
        self.type = type

    def calcR(self):
        self.r = ohm.calcR(self.v, self.i)

    def calcV(self):
        self.v = ohm.calcV(self.i, self.r)

    def calcI(self):
        self.i = ohm.calcI(self.v, self.r)

    def getName(self):
        return name + str(id)

class supply(comp):
    def __init__(self, id, v=1):
        super().__init__("P", id, s)
        self.v = v

class resistor(comp):
    def __init__(self, id, r=1):
        super().__init__("R", id, r)
        self.r = r

class diode(comp):
    vreq = 1
    def __init__(self, id, vreq, v=1):
        super().__init__("D", id, d)
        self.v = v
        self.vreq = vreq

    def canRun(self):#will the diode let the flow through
        return self.v >= self.vreq