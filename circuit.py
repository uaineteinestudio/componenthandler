import ohm
import component
import numpy as np

class cir:
  name="untitled"
  comps = []                    #empty array of components
  conMatrix = np.zeros((0,0))   #single direction connectivity of nodes
  def __init__(self, name, conMatrix, comps):
    self.name = name
    self.connectivity=conMatrix
    self.comps = comps
  
  def extendCon(self):
      n = self.n()
      newl = n+1
      newCon = np.zeros((newl, newl))
      newCon[0:n-1,0:n-1]=self.conMatrix    #copy over all but the last 2 thanks
      self.conMatrix = np.copy(newCon)

  def addComponent(self, newComp, connectsTo, connectsFrom):
    self.extendCon()
    self.comps.append(newComp)
    self.conMatrix[newComp.id, connectsTo] = 1
    self.conMatrix[connectsFrom, newComp.id] = 1

  def n(self):
      return len(self.comps)

  def printConMatrix(self):
      print(self.conMatrix)
  
  def solveMissingInfo(self):
      totalV = 0
      totalR = 0
      for i, comp in enumerate(self.comps):
          if (comp.type == component.s):    #check if this is a supply
              totalV += comp.v              #
          if (comp.type == component.r):    #could be resistor
              totalR += comp.r              #
      cur = ohm.calcI(totalV, totalR)       #what is the current?
      for i in range(0, self.n()):
          self.comps[i].i = cur

class subcir(cir):  #subcircuit for all linear sections
    vsupply = -1    #... meaning no parrallel
    def __init__(self, components, connect, vsupply, subid):
        self.comps = components
        self.connectivity = connect
        self.name = "subcircuit" + str(subid)
        self.vsupply = vsupply