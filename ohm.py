def calcR(v, i):#v=ir from Ohm's Law
    return v/i

def calcV(i, r):
    return i*r

def calcI(v, r):
    return v/r

def calcP(v, i):
    return v*i